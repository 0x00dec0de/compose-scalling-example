FROM golang:onbuild
ADD main /
EXPOSE 8888
ENTRYPOINT ["/main"]
